package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SmartParkingBoyTest {
    @Test
    void should_park_car_to_the_1st_parking_lot_when_smartParkingBoy_park_and_the_1st_parking_lot_have_more_empty_position_given_a_car() {
        // Given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(5);
        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);
        // When
        Ticket ticket = smartParkingBoy.park(new Car());
        // Then
        Assertions.assertNotNull(parkingLot1.fetch(ticket));
    }

    @Test
    void should_park_car_to_the_2nd_parking_lot_when_smartParkingBoy_park_and_the_2nd_parking_lot_have_more_empty_position_given_a_car() {
        // Given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);
        // When
        Ticket ticket = smartParkingBoy.park(new Car());
        // Then
        Assertions.assertNotNull(parkingLot2.fetch(ticket));
    }

    @Test
    void should_return_right_cars_when_smarkParkingBoy_fetch_twice_given_two_different_ticket() {
        // Given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(5);
        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        // When
        Ticket ticket1 = smartParkingBoy.park(car1);
        Ticket ticket2 = smartParkingBoy.park(car2);
        // Then
        Assertions.assertNotNull(parkingLot1.isValidTicket(ticket1));
        Assertions.assertNotNull(parkingLot2.isValidTicket(ticket2));
        Assertions.assertEquals(car1, smartParkingBoy.fetch(ticket1));
        Assertions.assertEquals(car2, smartParkingBoy.fetch(ticket2));
    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_smarkParkingBoy_fetch_given_a_wrong_ticket() {
        // Given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(5);
        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);
        // When
        smartParkingBoy.park(new Car());
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_smarkParkingBoy_fetch_twice_given_a_used_ticket() {
        // Given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(5);
        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);
        // When
        Ticket ticket = smartParkingBoy.park(new Car());
        smartParkingBoy.fetch(ticket);
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_NoAvailablePositionException_when_smarkParkingBoy_park_and_all_managed_parking_lot_is_full_given_a_car() {
        // Given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);
        smartParkingBoy.park(new Car());
        smartParkingBoy.park(new Car());
        // When
        // Then
        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position.", exception.getMessage());
    }
}
