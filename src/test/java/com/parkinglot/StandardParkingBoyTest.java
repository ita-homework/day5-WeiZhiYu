package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StandardParkingBoyTest {
    @Test
    void should_return_a_ticket_when_standardParkingBoy_park_given_a_car() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        Car car = new Car();
        // When
        Ticket ticket = standardParkingBoy.park(car);
        // Then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_two_different_tickets_when_standardParkingBoy_park_two_cars_given_two_cars() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        Car car1 = new Car();
        Car car2 = new Car();
        // When
        Ticket ticket1 = standardParkingBoy.park(car1);
        Ticket ticket2 = standardParkingBoy.park(car2);
        //Then
        Assertions.assertNotEquals(ticket1, ticket2);
    }

    @Test
    void should_throw_NoAvailablePositionException_when_standardParkingBoy_park_a_full_parkingLot_given_a_car() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(0));
        Car car = new Car();
        // When
        // Then
        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(car));
        Assertions.assertEquals("No available position.", exception.getMessage());

    }

    @Test
    void should_return_a_car_when_standardParkingBoy_fecth_given_a_ticket() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        Car car = new Car();
        Ticket ticket = standardParkingBoy.park(car);
        // When
        Car fetchedCar = standardParkingBoy.fetch(ticket);
        // Then
        Assertions.assertEquals(car, fetchedCar);

    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_standardParkingBoy_fetch_given_a_wrong_ticket() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        Car car = new Car();
        Ticket correctTicket = standardParkingBoy.park(car);
        Ticket wrongTicket = new Ticket();
        // When
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_standardParkingBoy_fetch_twice_using_same_tickets_given_a_ticket() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        Car car = new Car();
        Ticket ticket = standardParkingBoy.park(car);
        // When
        standardParkingBoy.fetch(ticket);
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_park_car_to_fitst_parking_lot_and_return_a_ticket_when_standardParkingBoy_park_a_car_and_the_first_parking_lot_is_not_full_given_a_car() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.addParkingLot(parkingLot1);
        standardParkingBoy.addParkingLot(parkingLot2);
        Car car = new Car();
        // When
        Ticket ticket = standardParkingBoy.park(car);
        // Then
        Assertions.assertEquals(car, parkingLot1.fetch(ticket));

    }

    @Test
    void should_park_car_to_second_parking_lot_and_return_a_ticket_when_standardParkingBoy_park_a_car_and_the_first_parking_lot_is_full_given_a_car() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.addParkingLot(parkingLot1);
        standardParkingBoy.addParkingLot(parkingLot2);
        Car car = new Car();
        // When
        Ticket ticket = standardParkingBoy.park(car);
        // Then
        Assertions.assertEquals(car, parkingLot2.fetch(ticket));
    }

    @Test
    void should_throw_NoAvailablePositionException_when_standardParkingBoy_park_a_car_and_all_parking_lots_is_full_given_a_car() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        standardParkingBoy.addParkingLot(parkingLot1);
        standardParkingBoy.addParkingLot(parkingLot2);
        // When
        standardParkingBoy.park(new Car());
        standardParkingBoy.park(new Car());
        // Then
        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position.", exception.getMessage());

    }

    @Test
    void should_return_a_car_when_standardParkingBoy_fecth_car_parking_at_first_parking_lot_given_a_ticket() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.addParkingLot(parkingLot1);
        standardParkingBoy.addParkingLot(parkingLot2);
        // When
        Ticket ticket = standardParkingBoy.park(new Car());
        //Then
        Assertions.assertNotNull(parkingLot1.fetch(ticket));
    }

    @Test
    void should_return_a_car_when_standardParkingBoy_fecth_car_parking_at_second_parking_lot_given_a_ticket() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.addParkingLot(parkingLot1);
        standardParkingBoy.addParkingLot(parkingLot2);
        // When
        standardParkingBoy.park(new Car());
        Ticket ticket = standardParkingBoy.park(new Car());
        //Then
        Assertions.assertNotNull(parkingLot2.fetch(ticket));
    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_standardParkingBoy_fetch_given_a_ticket_invalidated_for_all_parking_lot() {
        // Given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        standardParkingBoy.addParkingLot(parkingLot1);
        standardParkingBoy.addParkingLot(parkingLot2);
        // When
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

}
