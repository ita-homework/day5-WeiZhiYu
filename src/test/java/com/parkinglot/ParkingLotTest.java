package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_a_ticket_when_park_given_a_car() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        // When
        Ticket ticket = parkingLot.park(car);
        // Then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_two_different_tickets_when_park_two_cars_given_two_cars() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();
        // When
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);
        //Then
        Assertions.assertNotEquals(ticket1, ticket2);
    }

    @Test
    void should_return_a_car_when_fecth_given_a_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        // When
        Car fetchedCar = parkingLot.fetch(ticket);
        // Then
        Assertions.assertEquals(car, fetchedCar);

    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_fetch_given_a_wrong_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        Ticket correctTicket = parkingLot.park(car);
        Ticket wrongTicket = new Ticket();
        // When
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_fetch_twice_using_same_tickets_given_a_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        // When
        parkingLot.fetch(ticket);
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_null_when_park_a_full_parkingLot_given_a_car() {
        // Given
        ParkingLot parkingLot = new ParkingLot(0);
        Car car = new Car();
        // When
        // Then
        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car));
        Assertions.assertEquals("No available position.", exception.getMessage());

    }
}
