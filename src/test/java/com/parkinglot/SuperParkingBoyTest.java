package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SuperParkingBoyTest {
    @Test
    void should_park_car_to_the_1st_parking_lot_when_superParkingBoy_park_and_the_1st_parking_lot_have_larger_empty_position_rate_given_a_car() {
        // Given
        SuperParkingBoy SuperParkingBoy = new SuperParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(4);
        SuperParkingBoy.addParkingLot(parkingLot1);
        SuperParkingBoy.addParkingLot(parkingLot2);
        SuperParkingBoy.park(new Car());
        SuperParkingBoy.park(new Car());
        // When
        Ticket ticket = SuperParkingBoy.park(new Car());
        // Then
        Assertions.assertNotNull(parkingLot1.fetch(ticket));
    }

    @Test
    void should_park_car_to_the_2nd_parking_lot_when_superParkingBoy_park_and_the_2nd_parking_lot_have_larger_empty_position_rate_given_a_car() {
        // Given
        SuperParkingBoy SuperParkingBoy = new SuperParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(4);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SuperParkingBoy.addParkingLot(parkingLot1);
        SuperParkingBoy.addParkingLot(parkingLot2);
        SuperParkingBoy.park(new Car());
        SuperParkingBoy.park(new Car());
        // When
        Ticket ticket = SuperParkingBoy.park(new Car());
        // Then
        Assertions.assertNotNull(parkingLot2.fetch(ticket));
    }

    @Test
    void should_return_right_cars_when_superParkingBoy_fetch_twice_given_two_different_ticket() {
        // Given
        SuperParkingBoy SuperParkingBoy = new SuperParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SuperParkingBoy.addParkingLot(parkingLot1);
        SuperParkingBoy.addParkingLot(parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        // When
        Ticket ticket1 = SuperParkingBoy.park(car1);
        Ticket ticket2 = SuperParkingBoy.park(car2);
        // Then
        Assertions.assertNotNull(parkingLot1.isValidTicket(ticket1));
        Assertions.assertNotNull(parkingLot2.isValidTicket(ticket2));
        Assertions.assertEquals(car1, SuperParkingBoy.fetch(ticket1));
        Assertions.assertEquals(car2, SuperParkingBoy.fetch(ticket2));
    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_superParkingBoy_fetch_given_a_wrong_ticket() {
        // Given
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(5);
        superParkingBoy.addParkingLot(parkingLot1);
        superParkingBoy.addParkingLot(parkingLot2);
        // When
        superParkingBoy.park(new Car());
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> superParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_UnrecognizedParkingTicketException_when_superParkingBoy_fetch_twice_given_a_used_ticket() {
        // Given
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(5);
        superParkingBoy.addParkingLot(parkingLot1);
        superParkingBoy.addParkingLot(parkingLot2);
        // When
        Ticket ticket = superParkingBoy.park(new Car());
        superParkingBoy.fetch(ticket);
        // Then
        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> superParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_NoAvailablePositionException_when_superParkingBoy_park_and_all_managed_parking_lot_is_full_given_a_car() {
        // Given
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        superParkingBoy.addParkingLot(parkingLot1);
        superParkingBoy.addParkingLot(parkingLot2);
        superParkingBoy.park(new Car());
        superParkingBoy.park(new Car());
        // When
        // Then
        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> superParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position.", exception.getMessage());

    }
}
