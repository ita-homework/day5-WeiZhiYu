package com.parkinglot;

import com.parkinglot.Exception.ManagedParkingBoyFailedToDoOperationException;
import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnrecognizedParkingBoyException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotManagerTest {
    @Test
    void should_return_a_ticket_when_parkingLotManager_park_given_a_car() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingLot(new ParkingLot(10));
        // When
        Ticket ticket = parkingLotManager.park(new Car());
        // Then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_parkingLotManager_fetch_given_a_ticket() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingLot(new ParkingLot(10));
        Car car = new Car();
        // When
        Ticket ticket = parkingLotManager.park(car);
        // Then
        Assertions.assertEquals(car, parkingLotManager.fetch(ticket));
    }

    @Test
    void should_add_standardParkingBoy_to_list_when_parkingLotManager_addParkingBoy_given_a_standardParkingBoy() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        // When
        parkingLotManager.addParkingBoy(standardParkingBoy);
        // Then
        Assertions.assertTrue(parkingLotManager.hasParkingBoys(standardParkingBoy));
    }

    @Test
    void should_add_smartParkingBoy_to_list_when_parkingLotManager_addParkingBoy_given_a_smartParkingBoy() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        // When
        parkingLotManager.addParkingBoy(smartParkingBoy);
        // Then
        Assertions.assertTrue(parkingLotManager.hasParkingBoys(smartParkingBoy));
    }

    @Test
    void should_add_superParkingBoy_to_list_when_parkingLotManager_addParkingBoy_given_a_superParkingBoy() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        // When
        parkingLotManager.addParkingBoy(superParkingBoy);
        // Then
        Assertions.assertTrue(parkingLotManager.hasParkingBoys(superParkingBoy));
    }

    @Test
    void should_return_a_ticket_when_parkingLotManager_specifyParkingBoyToPark_given_a_parkingBoy_and_a_car() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        parkingLotManager.addParkingBoy(standardParkingBoy);
        // When
        Ticket ticket = parkingLotManager.specifyParkingBoyToPark(standardParkingBoy, new Car());
        // Then
        Assertions.assertNotNull(ticket);

    }

    @Test
    void should_throw_UnrecognizedParkingBoyException_when_parkingLotManager_specifyParkingBoyToPark_given_a_parkingBoy_not_in_list_and_a_car() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        // When
        // Then
        Exception exception = Assertions.assertThrows(
                UnrecognizedParkingBoyException.class,
                () -> parkingLotManager.specifyParkingBoyToPark(new StandardParkingBoy(), new Car())
        );
        Assertions.assertEquals("Unrecognized parking boy.", exception.getMessage());
    }

    @Test
    void should_return_a_car_when_parkingLotManager_specifyParkingBoyToFetch_given_a_parkingBoy_and_a_ticket() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        parkingLotManager.addParkingBoy(standardParkingBoy);
        Ticket ticket = parkingLotManager.specifyParkingBoyToPark(standardParkingBoy, new Car());
        // When
        // Then
        Assertions.assertNotNull(parkingLotManager.specifyParkingBoyToFetch(standardParkingBoy, ticket));
    }

    @Test
    void should_throw_UnrecognizedParkingBoyException_when_parkingLotManager_specifyParkingBoyToFetch_given_a_parkingBoy_not_in_list_and_a_ticket() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        Ticket ticket = standardParkingBoy.park(new Car());
        // When
        // Then
        Exception exception = Assertions.assertThrows(
                UnrecognizedParkingBoyException.class,
                () -> parkingLotManager.specifyParkingBoyToFetch(standardParkingBoy, ticket)
        );
        Assertions.assertEquals("Unrecognized parking boy.", exception.getMessage());
    }

    @Test
    void should_throw_ManagedParkingBoyFailedToDoOperationException_when_parkingLotManager_specifyParkingBoyToPark_and_failed_given_a_parkingBoy_and_a_car() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(1));
        parkingLotManager.addParkingBoy(standardParkingBoy);
        parkingLotManager.specifyParkingBoyToPark(standardParkingBoy, new Car());
        // When
        // Then
        Exception exception = Assertions.assertThrows(
                ManagedParkingBoyFailedToDoOperationException.class,
                () -> parkingLotManager.specifyParkingBoyToPark(standardParkingBoy, new Car())
        );
        Assertions.assertEquals("The parking boy failed to park car: No available position.", exception.getMessage());
    }


    @Test
    void should_throw_ManagedParkingBoyFailedToDoOperationException_when_parkingLotManager_specifyParkingBoyToFetch_and_failed_given_a_parkingBoy_and_a_ticket() {
        // Given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(1));
        parkingLotManager.addParkingBoy(standardParkingBoy);
        parkingLotManager.specifyParkingBoyToPark(standardParkingBoy, new Car());
        // When
        // Then
        Exception exception = Assertions.assertThrows(
                ManagedParkingBoyFailedToDoOperationException.class,
                () -> parkingLotManager.specifyParkingBoyToFetch(standardParkingBoy, new Ticket())
        );
        Assertions.assertEquals("The parking boy failed to fetch car: Unrecognized parking ticket.", exception.getMessage());

    }

}
