package com.parkinglot;

import com.parkinglot.Exception.UnrecognizedParkingTicketException;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy {
    protected List<ParkingLot> managedParkingLot = new ArrayList<>();

    public ParkingBoy() {}

    public ParkingBoy(ParkingLot parkingLot) {
        managedParkingLot.add(parkingLot);
    }

    public Ticket park(Car car) { return null;}

    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot: managedParkingLot) {
            if (parkingLot.isValidTicket(ticket)) {
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedParkingTicketException("Unrecognized parking ticket.");
    }

    public void addParkingLot(ParkingLot parkingLot) {
        managedParkingLot.add(parkingLot);
    }
}
