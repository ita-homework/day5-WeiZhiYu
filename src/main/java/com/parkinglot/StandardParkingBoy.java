package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnrecognizedParkingTicketException;

import java.util.ArrayList;
import java.util.List;

public class StandardParkingBoy extends ParkingBoy{

    public StandardParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }

    public StandardParkingBoy() {}

    @Override
    public Ticket park(Car car) {
        for (ParkingLot parkingLot: managedParkingLot) {
            if (!parkingLot.isFull()) {
                return parkingLot.park(car);
            }
        }
        throw new NoAvailablePositionException("No available position.");
    }
}
