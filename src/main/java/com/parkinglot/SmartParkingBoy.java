package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;

public class SmartParkingBoy extends ParkingBoy{
    public SmartParkingBoy() {}

    public SmartParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot parkingLotHasMoreEmptyPositions = null;
        for (ParkingLot parkingLot: managedParkingLot) {
            if (parkingLotHasMoreEmptyPositions == null || parkingLotHasMoreEmptyPositions.getEmptyPositionCount() < parkingLot.getEmptyPositionCount()) {
                parkingLotHasMoreEmptyPositions = parkingLot;

            }
        }
        if (parkingLotHasMoreEmptyPositions != null && !parkingLotHasMoreEmptyPositions.isFull()) {
            return parkingLotHasMoreEmptyPositions.park(car);
        }
        throw new NoAvailablePositionException("No available position.");
    }
}
