package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;

public class SuperParkingBoy extends ParkingBoy{
    public SuperParkingBoy() {
    }

    public SuperParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot parkingLotHasLargerEmptyPositionsRate = null;
        for (ParkingLot parkingLot: managedParkingLot) {
            if (parkingLotHasLargerEmptyPositionsRate == null ||
                    parkingLotHasLargerEmptyPositionsRate.getAvailablePositionRate() < parkingLot.getAvailablePositionRate()) {
                parkingLotHasLargerEmptyPositionsRate = parkingLot;

            }
        }
        if (parkingLotHasLargerEmptyPositionsRate != null && !parkingLotHasLargerEmptyPositionsRate.isFull()) {
            return parkingLotHasLargerEmptyPositionsRate.park(car);
        }
        throw new NoAvailablePositionException("No available position.");
    }
}
