package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnrecognizedParkingTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Integer capacity;
    private Map<Ticket, Car> parkedCars = new HashMap<>();

    public ParkingLot(Integer capacity) {
        this.capacity = capacity;
    }

    public Ticket park(Car car) {
        if (parkedCars.size() >= capacity) {
            throw new NoAvailablePositionException("No available position.");
        }

        Ticket ticket = new Ticket();
        parkedCars.put(ticket, car);

        return ticket;
    }

    public Car fetch(Ticket ticket) {
        Car fetchedCar = parkedCars.get(ticket);
        if (fetchedCar == null) {
            throw new UnrecognizedParkingTicketException("Unrecognized parking ticket.");
        }
        parkedCars.remove(ticket);
        return fetchedCar;
    }

    public Boolean isFull() {
        return parkedCars.size() >= capacity;
    }

    public Boolean isValidTicket(Ticket ticket) {
        Car fetchedCar = parkedCars.get(ticket);
        return fetchedCar != null;
    }

    public Integer getEmptyPositionCount() {
        return capacity - parkedCars.size();
    }

    public Double getAvailablePositionRate() {
        if (capacity == null || capacity == 0) {
            throw new RuntimeException("The parking lot's capacity is null/zero!");
        }
        return Double.valueOf(getEmptyPositionCount()) / capacity;
    }
}
