package com.parkinglot;

import com.parkinglot.Exception.ManagedParkingBoyFailedToDoOperationException;
import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnrecognizedParkingBoyException;
import com.parkinglot.Exception.UnrecognizedParkingTicketException;

import java.util.ArrayList;
import java.util.List;

public class ParkingLotManager extends StandardParkingBoy{
    private List<ParkingBoy> managedParkingBoys = new ArrayList<>();
    public ParkingLotManager(ParkingLot parkingLot) {
        super(parkingLot);
    }

    public ParkingLotManager() {
    }

    public void addParkingBoy(ParkingBoy parkingBoy) {
        if (!hasParkingBoys(parkingBoy)) {
            managedParkingBoys.add(parkingBoy);
        }
    }

    public boolean hasParkingBoys(ParkingBoy parkingBoy) {
        return managedParkingBoys.contains(parkingBoy);
    }

    public Ticket specifyParkingBoyToPark(ParkingBoy parkingBoy, Car car) {
        if (!hasParkingBoys(parkingBoy)) {
            throw new UnrecognizedParkingBoyException("Unrecognized parking boy.");
        }
        try {
            return parkingBoy.park(car);
        } catch (NoAvailablePositionException exception) {
            throw new ManagedParkingBoyFailedToDoOperationException("The parking boy failed to park car: " + exception.getMessage());
        }
    }

    public Car specifyParkingBoyToFetch(ParkingBoy parkingBoy, Ticket ticket) {
        if (!hasParkingBoys(parkingBoy)) {
            throw new UnrecognizedParkingBoyException("Unrecognized parking boy.");
        }
        try {
            return parkingBoy.fetch(ticket);
        } catch (UnrecognizedParkingTicketException exception) {
            throw new ManagedParkingBoyFailedToDoOperationException("The parking boy failed to fetch car: " + exception.getMessage());
        }
    }
}
