package com.parkinglot.Exception;

public class UnrecognizedParkingBoyException extends RuntimeException{
    public UnrecognizedParkingBoyException(String message) {
        super(message);
    }
}
