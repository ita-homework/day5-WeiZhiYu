package com.parkinglot.Exception;

public class UnrecognizedParkingTicketException extends RuntimeException{
    public UnrecognizedParkingTicketException(String message) {
        super(message);
    }
}
