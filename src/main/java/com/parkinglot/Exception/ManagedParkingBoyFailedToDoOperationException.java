package com.parkinglot.Exception;

public class ManagedParkingBoyFailedToDoOperationException extends RuntimeException{
    public ManagedParkingBoyFailedToDoOperationException(String message) {
        super(message);
    }
}
