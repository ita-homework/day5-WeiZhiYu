# O
- Code Review: At this part, our group share a code using command pattern. But the teacher pointed out the error in code, and we realize our misunderstanding about command pattern. I get a deeper understand about command pattern.
- TDD practice: we do TDD practice whole day. I can keep up with the pace of practice in class. But sometimes I forgot to write test before implement function, and have to write test later. This is not a good behavior, need to be corrected.

# R
nice!

# I
- Design patterns are difficult to learn and need to be studied carefully and understood correctly

# D
I will do more practice to improve my coding skill